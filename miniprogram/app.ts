// app.ts
App<IAppOption>({
  globalData: {},
  onLaunch() {
    // 展示本地存储能力
    // const logs = wx.getStorageSync('logs') || []
    // logs.unshift(Date.now())
    // wx.setStorageSync('logs', logs)

    // 登录
    wx.login({
      success: res => {
    wx.request({
      url: 'http://47.102.117.224:8080/get-open-id',
      data: {
         //填上自己的小程序唯一标识
        appid: 'wx9b4f90222864d257',
         //填上自己的小程序的 app secret
        secret: '5a850cbc6357418299f902be86eff408',
        code: res.code
      },
      method: 'POST',
      header: { 'content-type': 'application/json'},
      success: function(openIdRes:any){
          console.log(res.code,openIdRes.data);

          const data = JSON.parse(openIdRes.data.data)
          
          if (data.openid) {
            wx.setStorageSync('openid', data.openid)
          } else {
            wx.showToast({
              title: data.errmsg,
              icon: 'none',
              duration: 2000
            })
          }
         
      },
      fail: function(error) {
          console.info("获取用户openId失败");
          console.info(error);
      }
   })
  },
})
    // 获取用户信息
    wx.getSetting({
      success: res => {
        if (res.authSetting['scope.userInfo']) {
          // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
          wx.getUserInfo({
            success: res => {
              // 可以将 res 发送给后台解码出 unionId
              this.globalData.userInfo = res.userInfo

              // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
              // 所以此处加入 callback 以防止这种情况
              if (this.userInfoReadyCallback) {
                this.userInfoReadyCallback(res)
              }
            },
          })
        }
      },
    })
  },
})