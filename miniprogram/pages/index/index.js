"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Page({
    data: {
        recommend: {},
        bookList: [],
        showBar: false,
        checkAll: false
    },
    onLoad: function () {
        this.getList();
    },
    getList: function () {
        var _this_1 = this;
        var openid = wx.getStorageSync('openid');
        if (!openid) {
            wx.showToast({
                title: '未登录，请先登录',
                icon: 'none',
                duration: 2000
            });
            return;
        }
        wx.request({
            method: "POST",
            url: 'http://47.102.117.224:8080/user-book',
            header: {
                'content-type': 'application/json'
            },
            data: { openid: openid },
            success: function (res) {
                if (!res.data.data || res.data.data.length <= 0) {
                    _this_1.setData({ bookList: [], recommend: {} });
                }
                else {
                    _this_1.setData({ bookList: res.data.data, recommend: res.data.data[res.data.data.length - 1] });
                }
            }
        });
    },
    selectBookId: function (e) {
        var id = e.currentTarget.dataset.id;
        var bookList = this.data.bookList;
        var v = bookList.findIndex(function (v) { return v.id === id; });
        if (!!bookList[v].selected) {
            bookList[v].selected = false;
        }
        else {
            bookList[v].selected = true;
        }
        if (!this.data.showBar) {
            this.setData({ showBar: true });
        }
        this.setData({ bookList: bookList });
        console.warn(this.data.bookList);
    },
    checkboxChange: function () {
        this.setData({ checkAll: !this.data.checkAll });
        if (this.data.checkAll) {
            var bookList = this.data.bookList.map(function (v) { return (__assign({}, v, { selected: true })); });
            this.setData({ bookList: bookList });
        }
        else {
            var bookList = this.data.bookList.map(function (v) { return (__assign({}, v, { selected: false })); });
            this.setData({ bookList: bookList });
        }
    },
    delete: function () {
        var _this = this;
        var ids = this.data.bookList.filter(function (v) { return v.selected; }).map(function (v) { return v.id; });
        wx.request({
            method: "POST",
            url: 'http://47.102.117.224:8080/delete-user-book',
            header: {
                'content-type': 'application/json'
            },
            data: { ids: ids },
            success: function (res) {
                if (res.data.meta.err_code !== 0) {
                    wx.showToast({
                        title: '移除出书架失败，失败原因：' + res.data.meta.err_msg,
                        icon: 'none',
                        duration: 2000
                    });
                }
                else {
                    wx.showToast({
                        title: '成功移除出书架',
                        icon: 'success',
                        duration: 2000
                    });
                    _this.getList();
                }
            }
        });
    },
    complete: function () {
        this.setData({ showBar: false });
        var bookList = this.data.bookList.map(function (v) { return (__assign({}, v, { selected: false })); });
        this.setData({ bookList: bookList });
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7QUFhQSxJQUFJLENBQUM7SUFDSCxJQUFJLEVBQUU7UUFDSixTQUFTLEVBQUUsRUFBRTtRQUNiLFFBQVEsRUFBRSxFQUFFO1FBQ1osT0FBTyxFQUFDLEtBQUs7UUFDYixRQUFRLEVBQUMsS0FBSztLQUNmO0lBQ0QsTUFBTTtRQUNKLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQTtJQUNoQixDQUFDO0lBRUQsT0FBTztRQUFQLG1CQXlCQztRQXhCQyxJQUFJLE1BQU0sR0FBRyxFQUFFLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxDQUFBO1FBQ3hDLElBQUcsQ0FBQyxNQUFNLEVBQUU7WUFDVixFQUFFLENBQUMsU0FBUyxDQUFDO2dCQUNYLEtBQUssRUFBRSxVQUFVO2dCQUNqQixJQUFJLEVBQUUsTUFBTTtnQkFDWixRQUFRLEVBQUUsSUFBSTthQUNmLENBQUMsQ0FBQTtZQUNGLE9BQU87U0FDUjtRQUNELEVBQUUsQ0FBQyxPQUFPLENBQUM7WUFDVCxNQUFNLEVBQUMsTUFBTTtZQUNiLEdBQUcsRUFBRSxzQ0FBc0M7WUFDM0MsTUFBTSxFQUFFO2dCQUNOLGNBQWMsRUFBRSxrQkFBa0I7YUFDbkM7WUFDRCxJQUFJLEVBQUUsRUFBQyxNQUFNLFFBQUEsRUFBQztZQUNkLE9BQU8sRUFBRyxVQUFDLEdBQWdCO2dCQUN6QixJQUFHLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLElBQUksR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxJQUFHLENBQUMsRUFBRTtvQkFDN0MsT0FBSSxDQUFDLE9BQU8sQ0FBQyxFQUFDLFFBQVEsRUFBQyxFQUFFLEVBQUMsU0FBUyxFQUFDLEVBQUUsRUFBQyxDQUFDLENBQUE7aUJBQ3pDO3FCQUFNO29CQUNMLE9BQUksQ0FBQyxPQUFPLENBQUMsRUFBQyxRQUFRLEVBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUMsU0FBUyxFQUFFLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBQyxDQUFDLENBQUMsRUFBQyxDQUFDLENBQUE7aUJBQ3hGO1lBQ0gsQ0FBQztTQUNGLENBQUMsQ0FBQTtJQUNKLENBQUM7SUFDRCxZQUFZLFlBQUMsQ0FBQztRQUNaLElBQU0sRUFBRSxHQUFHLENBQUMsQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQztRQUN0QyxJQUFNLFFBQVEsR0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQztRQUN4QyxJQUFNLENBQUMsR0FBRyxRQUFRLENBQUMsU0FBUyxDQUFDLFVBQUEsQ0FBQyxJQUFHLE9BQUEsQ0FBQyxDQUFDLEVBQUUsS0FBSyxFQUFFLEVBQVgsQ0FBVyxDQUFDLENBQUE7UUFDN0MsSUFBSSxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsRUFBRTtZQUMxQixRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztTQUM5QjthQUFNO1lBQ0wsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7U0FDN0I7UUFDRCxJQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUU7WUFDckIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxFQUFDLE9BQU8sRUFBQyxJQUFJLEVBQUMsQ0FBQyxDQUFBO1NBQzdCO1FBQ0QsSUFBSSxDQUFDLE9BQU8sQ0FBQyxFQUFDLFFBQVEsVUFBQSxFQUFDLENBQUMsQ0FBQTtRQUN4QixPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUE7SUFDbEMsQ0FBQztJQUNELGNBQWM7UUFDWixJQUFJLENBQUMsT0FBTyxDQUFDLEVBQUMsUUFBUSxFQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUMsQ0FBQyxDQUFBO1FBQzVDLElBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUU7WUFDckIsSUFBTSxRQUFRLEdBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFnQixDQUFDLEdBQUcsQ0FBQyxVQUFBLENBQUMsSUFBSSxPQUFBLGNBQUssQ0FBQyxJQUFDLFFBQVEsRUFBQyxJQUFJLElBQUUsRUFBdEIsQ0FBc0IsQ0FBQyxDQUFDO1lBQzlFLElBQUksQ0FBQyxPQUFPLENBQUMsRUFBQyxRQUFRLFVBQUEsRUFBQyxDQUFDLENBQUE7U0FDekI7YUFBTTtZQUNMLElBQU0sUUFBUSxHQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBZ0IsQ0FBQyxHQUFHLENBQUMsVUFBQSxDQUFDLElBQUksT0FBQSxjQUFLLENBQUMsSUFBQyxRQUFRLEVBQUMsS0FBSyxJQUFFLEVBQXZCLENBQXVCLENBQUMsQ0FBQztZQUMvRSxJQUFJLENBQUMsT0FBTyxDQUFDLEVBQUMsUUFBUSxVQUFBLEVBQUMsQ0FBQyxDQUFBO1NBQ3pCO0lBQ0gsQ0FBQztJQUNELE1BQU07UUFDSixJQUFJLEtBQUssR0FBRyxJQUFJLENBQUM7UUFDakIsSUFBTSxHQUFHLEdBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFnQixDQUFDLE1BQU0sQ0FBQyxVQUFBLENBQUMsSUFBSSxPQUFBLENBQUMsQ0FBQyxRQUFRLEVBQVYsQ0FBVSxDQUFDLENBQUMsR0FBRyxDQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxDQUFDLEVBQUUsRUFBSixDQUFJLENBQUMsQ0FBQTtRQUM5RSxFQUFFLENBQUMsT0FBTyxDQUFDO1lBQ1QsTUFBTSxFQUFDLE1BQU07WUFDYixHQUFHLEVBQUUsNkNBQTZDO1lBQ2xELE1BQU0sRUFBRTtnQkFDTixjQUFjLEVBQUUsa0JBQWtCO2FBQ25DO1lBQ0QsSUFBSSxFQUFFLEVBQUMsR0FBRyxLQUFBLEVBQUM7WUFDWCxPQUFPLEVBQUcsVUFBQyxHQUFnQjtnQkFDekIsSUFBSSxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLEtBQUssQ0FBQyxFQUFFO29CQUNoQyxFQUFFLENBQUMsU0FBUyxDQUFDO3dCQUNYLEtBQUssRUFBRSxlQUFlLEdBQUUsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTzt3QkFDN0MsSUFBSSxFQUFFLE1BQU07d0JBQ1osUUFBUSxFQUFFLElBQUk7cUJBQ2YsQ0FBQyxDQUFBO2lCQUNIO3FCQUFNO29CQUNMLEVBQUUsQ0FBQyxTQUFTLENBQUM7d0JBQ1gsS0FBSyxFQUFFLFNBQVM7d0JBQ2hCLElBQUksRUFBRSxTQUFTO3dCQUNmLFFBQVEsRUFBRSxJQUFJO3FCQUNmLENBQUMsQ0FBQTtvQkFDRixLQUFLLENBQUMsT0FBTyxFQUFFLENBQUE7aUJBQ2hCO1lBQ0gsQ0FBQztTQUNGLENBQUMsQ0FBQTtJQUNKLENBQUM7SUFDRCxRQUFRO1FBQ04sSUFBSSxDQUFDLE9BQU8sQ0FBQyxFQUFDLE9BQU8sRUFBQyxLQUFLLEVBQUMsQ0FBQyxDQUFBO1FBQzdCLElBQU0sUUFBUSxHQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBZ0IsQ0FBQyxHQUFHLENBQUMsVUFBQSxDQUFDLElBQUksT0FBQSxjQUFLLENBQUMsSUFBQyxRQUFRLEVBQUMsS0FBSyxJQUFFLEVBQXZCLENBQXVCLENBQUMsQ0FBQztRQUMvRSxJQUFJLENBQUMsT0FBTyxDQUFDLEVBQUMsUUFBUSxVQUFBLEVBQUMsQ0FBQyxDQUFBO0lBQzFCLENBQUM7Q0FDRixDQUFDLENBQUEiLCJzb3VyY2VzQ29udGVudCI6WyIvLyBpbmRleC50c1xuXG5pbnRlcmZhY2UgQm9vayB7XG4gIGlkIDogICAgICAgICBzdHJpbmcgICAgXG5cdG5hbWUgOiAgICAgICBzdHJpbmcgXG5cdGF1dGhvcjogICAgICBzdHJpbmcgICAgXG5cdGltYWdlX3VybDogICAgc3RyaW5nIFxuICBkZXNjcmlwdGlvbiA6c3RyaW5nXG5cdGJvb2tfdHlwZTpudW1iZXJcblx0dXBkYXRlX3RpbWU6IERhdGVcbn1cblxuXG5QYWdlKHtcbiAgZGF0YToge1xuICAgIHJlY29tbWVuZDoge30sXG4gICAgYm9va0xpc3Q6IFtdLFxuICAgIHNob3dCYXI6ZmFsc2UsXG4gICAgY2hlY2tBbGw6ZmFsc2VcbiAgfSxcbiAgb25Mb2FkKCkge1xuICAgIHRoaXMuZ2V0TGlzdCgpXG4gIH0sXG4gIFxuICBnZXRMaXN0ICgpIHtcbiAgICB2YXIgb3BlbmlkID0gd3guZ2V0U3RvcmFnZVN5bmMoJ29wZW5pZCcpXG4gICAgaWYoIW9wZW5pZCkge1xuICAgICAgd3guc2hvd1RvYXN0KHtcbiAgICAgICAgdGl0bGU6ICfmnKrnmbvlvZXvvIzor7flhYjnmbvlvZUnLFxuICAgICAgICBpY29uOiAnbm9uZScsXG4gICAgICAgIGR1cmF0aW9uOiAyMDAwXG4gICAgICB9KVxuICAgICAgcmV0dXJuO1xuICAgIH1cbiAgICB3eC5yZXF1ZXN0KHtcbiAgICAgIG1ldGhvZDpcIlBPU1RcIixcbiAgICAgIHVybDogJ2h0dHA6Ly80Ny4xMDIuMTE3LjIyNDo4MDgwL3VzZXItYm9vaycsIFxuICAgICAgaGVhZGVyOiB7XG4gICAgICAgICdjb250ZW50LXR5cGUnOiAnYXBwbGljYXRpb24vanNvbicgLy8g6buY6K6k5YC8XG4gICAgICB9LFxuICAgICAgZGF0YToge29wZW5pZH0sXG4gICAgICBzdWNjZXNzOiAgKHJlczoge2RhdGE6IGFueX0pID0+IHtcbiAgICAgICAgaWYoIXJlcy5kYXRhLmRhdGEgfHwgcmVzLmRhdGEuZGF0YS5sZW5ndGggPD0wKSB7XG4gICAgICAgICAgdGhpcy5zZXREYXRhKHtib29rTGlzdDpbXSxyZWNvbW1lbmQ6e319KVxuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHRoaXMuc2V0RGF0YSh7Ym9va0xpc3Q6cmVzLmRhdGEuZGF0YSxyZWNvbW1lbmQ6IHJlcy5kYXRhLmRhdGFbcmVzLmRhdGEuZGF0YS5sZW5ndGgtMV19KVxuICAgICAgICB9XG4gICAgICB9XG4gICAgfSlcbiAgfSxcbiAgc2VsZWN0Qm9va0lkKGUpIHtcbiAgICBjb25zdCBpZCA9IGUuY3VycmVudFRhcmdldC5kYXRhc2V0LmlkO1xuICAgIGNvbnN0IGJvb2tMaXN0OmFueSA9IHRoaXMuZGF0YS5ib29rTGlzdDtcbiAgICBjb25zdCB2ID0gYm9va0xpc3QuZmluZEluZGV4KHYgPT52LmlkID09PSBpZClcbiAgICBpZiAoISFib29rTGlzdFt2XS5zZWxlY3RlZCkge1xuICAgICAgYm9va0xpc3Rbdl0uc2VsZWN0ZWQgPSBmYWxzZTtcbiAgICB9IGVsc2Uge1xuICAgICAgYm9va0xpc3Rbdl0uc2VsZWN0ZWQgPSB0cnVlO1xuICAgIH1cbiAgICBpZighdGhpcy5kYXRhLnNob3dCYXIpIHtcbiAgICAgIHRoaXMuc2V0RGF0YSh7c2hvd0Jhcjp0cnVlfSlcbiAgICB9XG4gICAgdGhpcy5zZXREYXRhKHtib29rTGlzdH0pXG4gICAgY29uc29sZS53YXJuKHRoaXMuZGF0YS5ib29rTGlzdClcbiAgfSxcbiAgY2hlY2tib3hDaGFuZ2UoKSB7XG4gICAgdGhpcy5zZXREYXRhKHtjaGVja0FsbDohdGhpcy5kYXRhLmNoZWNrQWxsfSlcbiAgICBpZih0aGlzLmRhdGEuY2hlY2tBbGwpIHtcbiAgICAgIGNvbnN0IGJvb2tMaXN0ID0gKHRoaXMuZGF0YS5ib29rTGlzdCBhcyBhbnkpLm1hcCh2ID0+ICh7Li4udixzZWxlY3RlZDp0cnVlfSkpO1xuICAgICAgdGhpcy5zZXREYXRhKHtib29rTGlzdH0pXG4gICAgfSBlbHNlIHtcbiAgICAgIGNvbnN0IGJvb2tMaXN0ID0gKHRoaXMuZGF0YS5ib29rTGlzdCBhcyBhbnkpLm1hcCh2ID0+ICh7Li4udixzZWxlY3RlZDpmYWxzZX0pKTtcbiAgICAgIHRoaXMuc2V0RGF0YSh7Ym9va0xpc3R9KVxuICAgIH1cbiAgfSxcbiAgZGVsZXRlKCkge1xuICAgIHZhciBfdGhpcyA9IHRoaXM7XG4gICAgY29uc3QgaWRzID0gKHRoaXMuZGF0YS5ib29rTGlzdCBhcyBhbnkpLmZpbHRlcih2ID0+IHYuc2VsZWN0ZWQpLm1hcCh2ID0+IHYuaWQpXG4gICAgd3gucmVxdWVzdCh7XG4gICAgICBtZXRob2Q6XCJQT1NUXCIsXG4gICAgICB1cmw6ICdodHRwOi8vNDcuMTAyLjExNy4yMjQ6ODA4MC9kZWxldGUtdXNlci1ib29rJyxcbiAgICAgIGhlYWRlcjoge1xuICAgICAgICAnY29udGVudC10eXBlJzogJ2FwcGxpY2F0aW9uL2pzb24nIC8vIOm7mOiupOWAvFxuICAgICAgfSxcbiAgICAgIGRhdGE6IHtpZHN9LFxuICAgICAgc3VjY2VzczogIChyZXM6IHtkYXRhOiBhbnl9KSA9PiB7XG4gICAgICAgIGlmIChyZXMuZGF0YS5tZXRhLmVycl9jb2RlICE9PSAwKSB7XG4gICAgICAgICAgd3guc2hvd1RvYXN0KHtcbiAgICAgICAgICAgIHRpdGxlOiAn56e76Zmk5Ye65Lmm5p625aSx6LSl77yM5aSx6LSl5Y6f5Zug77yaJysgcmVzLmRhdGEubWV0YS5lcnJfbXNnLFxuICAgICAgICAgICAgaWNvbjogJ25vbmUnLFxuICAgICAgICAgICAgZHVyYXRpb246IDIwMDBcbiAgICAgICAgICB9KVxuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHd4LnNob3dUb2FzdCh7XG4gICAgICAgICAgICB0aXRsZTogJ+aIkOWKn+enu+mZpOWHuuS5puaeticsXG4gICAgICAgICAgICBpY29uOiAnc3VjY2VzcycsXG4gICAgICAgICAgICBkdXJhdGlvbjogMjAwMFxuICAgICAgICAgIH0pXG4gICAgICAgICAgX3RoaXMuZ2V0TGlzdCgpXG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9KVxuICB9LFxuICBjb21wbGV0ZSgpIHtcbiAgICB0aGlzLnNldERhdGEoe3Nob3dCYXI6ZmFsc2V9KVxuICAgIGNvbnN0IGJvb2tMaXN0ID0gKHRoaXMuZGF0YS5ib29rTGlzdCBhcyBhbnkpLm1hcCh2ID0+ICh7Li4udixzZWxlY3RlZDpmYWxzZX0pKTtcbiAgICB0aGlzLnNldERhdGEoe2Jvb2tMaXN0fSlcbiAgfVxufSlcbiJdfQ==