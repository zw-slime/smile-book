// index.ts

interface Book {
  id :         string    
	name :       string 
	author:      string    
	image_url:    string 
  description :string
	book_type:number
	update_time: Date
}


Page({
  data: {
    recommend: {},
    bookList: [],
    showBar:false,
    checkAll:false
  },
  onLoad() {
    this.getList()
  },
  
  getList () {
    var openid = wx.getStorageSync('openid')
    if(!openid) {
      wx.showToast({
        title: '未登录，请先登录',
        icon: 'none',
        duration: 2000
      })
      return;
    }
    wx.request({
      method:"POST",
      url: 'http://47.102.117.224:8080/user-book', 
      header: {
        'content-type': 'application/json' // 默认值
      },
      data: {openid},
      success:  (res: {data: any}) => {
        if(!res.data.data || res.data.data.length <=0) {
          this.setData({bookList:[],recommend:{}})
        } else {
          this.setData({bookList:res.data.data,recommend: res.data.data[res.data.data.length-1]})
        }
      }
    })
  },
  selectBookId(e) {
    const id = e.currentTarget.dataset.id;
    const bookList:any = this.data.bookList;
    const v = bookList.findIndex(v =>v.id === id)
    if (!!bookList[v].selected) {
      bookList[v].selected = false;
    } else {
      bookList[v].selected = true;
    }
    if(!this.data.showBar) {
      this.setData({showBar:true})
    }
    this.setData({bookList})
    console.warn(this.data.bookList)
  },
  checkboxChange() {
    this.setData({checkAll:!this.data.checkAll})
    if(this.data.checkAll) {
      const bookList = (this.data.bookList as any).map(v => ({...v,selected:true}));
      this.setData({bookList})
    } else {
      const bookList = (this.data.bookList as any).map(v => ({...v,selected:false}));
      this.setData({bookList})
    }
  },
  delete() {
    var _this = this;
    const ids = (this.data.bookList as any).filter(v => v.selected).map(v => v.id)
    wx.request({
      method:"POST",
      url: 'http://47.102.117.224:8080/delete-user-book',
      header: {
        'content-type': 'application/json' // 默认值
      },
      data: {ids},
      success:  (res: {data: any}) => {
        if (res.data.meta.err_code !== 0) {
          wx.showToast({
            title: '移除出书架失败，失败原因：'+ res.data.meta.err_msg,
            icon: 'none',
            duration: 2000
          })
        } else {
          wx.showToast({
            title: '成功移除出书架',
            icon: 'success',
            duration: 2000
          })
          _this.getList()
        }
      }
    })
  },
  complete() {
    this.setData({showBar:false})
    const bookList = (this.data.bookList as any).map(v => ({...v,selected:false}));
    this.setData({bookList})
  }
})
