"use strict";
var app = getApp();
Page({
    data: {
        motto: 'Hello World',
        userInfo: {},
        hasUserInfo: false,
        canIUse: wx.canIUse('button.open-type.getUserInfo'),
    },
    bindViewTap: function () {
        wx.navigateTo({
            url: '../logs/logs',
        });
    },
    onLoad: function () {
        var _this = this;
        if (app.globalData.userInfo) {
            this.setData({
                userInfo: app.globalData.userInfo,
                hasUserInfo: true,
            });
        }
        else if (this.data.canIUse) {
            app.userInfoReadyCallback = function (res) {
                _this.setData({
                    userInfo: res.userInfo,
                    hasUserInfo: true,
                });
            };
        }
        else {
            wx.getUserInfo({
                success: function (res) {
                    app.globalData.userInfo = res.userInfo;
                    _this.setData({
                        userInfo: res.userInfo,
                        hasUserInfo: true,
                    });
                },
            });
        }
    },
    getUserInfo: function (e) {
        console.log(e);
        app.globalData.userInfo = e.detail.userInfo;
        this.setData({
            userInfo: e.detail.userInfo,
            hasUserInfo: true,
        });
    },
    login: function () {
        wx.login({
            success: function (res) {
                wx.request({
                    url: 'http://47.102.117.224:8080/get-open-id',
                    data: {
                        appid: 'wx9b4f90222864d257',
                        secret: '5a850cbc6357418299f902be86eff408',
                        code: res.code
                    },
                    method: 'POST',
                    header: { 'content-type': 'application/json' },
                    success: function (openIdRes) {
                        console.log(res.code, openIdRes.data);
                        var data = JSON.parse(openIdRes.data.data);
                        if (data.openid) {
                            wx.setStorageSync('openid', data.openid);
                        }
                        else {
                            wx.showToast({
                                title: data.errmsg,
                                icon: 'none',
                                duration: 2000
                            });
                        }
                    },
                    fail: function (error) {
                        console.info("获取用户openId失败");
                        console.info(error);
                    }
                });
            },
        });
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsSUFBTSxHQUFHLEdBQUcsTUFBTSxFQUFjLENBQUE7QUFDaEMsSUFBSSxDQUFDO0lBQ0gsSUFBSSxFQUFFO1FBQ0osS0FBSyxFQUFFLGFBQWE7UUFDcEIsUUFBUSxFQUFFLEVBQUU7UUFDWixXQUFXLEVBQUUsS0FBSztRQUNsQixPQUFPLEVBQUUsRUFBRSxDQUFDLE9BQU8sQ0FBQyw4QkFBOEIsQ0FBQztLQUN0RDtJQUNELFdBQVcsRUFBRTtRQUNULEVBQUUsQ0FBQyxVQUFVLENBQUM7WUFDVixHQUFHLEVBQUUsY0FBYztTQUN0QixDQUFDLENBQUM7SUFDUCxDQUFDO0lBQ0QsTUFBTSxFQUFFO1FBQ0osSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDO1FBQ2pCLElBQUksR0FBRyxDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQUU7WUFDekIsSUFBSSxDQUFDLE9BQU8sQ0FBQztnQkFDVCxRQUFRLEVBQUUsR0FBRyxDQUFDLFVBQVUsQ0FBQyxRQUFRO2dCQUNqQyxXQUFXLEVBQUUsSUFBSTthQUNwQixDQUFDLENBQUM7U0FDTjthQUNJLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUU7WUFDeEIsR0FBRyxDQUFDLHFCQUFxQixHQUFHLFVBQVUsR0FBRztnQkFDckMsS0FBSyxDQUFDLE9BQU8sQ0FBQztvQkFDVixRQUFRLEVBQUUsR0FBRyxDQUFDLFFBQVE7b0JBQ3RCLFdBQVcsRUFBRSxJQUFJO2lCQUNwQixDQUFDLENBQUM7WUFDUCxDQUFDLENBQUM7U0FDTDthQUNJO1lBQ0QsRUFBRSxDQUFDLFdBQVcsQ0FBQztnQkFDWCxPQUFPLEVBQUUsVUFBVSxHQUFHO29CQUNsQixHQUFHLENBQUMsVUFBVSxDQUFDLFFBQVEsR0FBRyxHQUFHLENBQUMsUUFBUSxDQUFDO29CQUN2QyxLQUFLLENBQUMsT0FBTyxDQUFDO3dCQUNWLFFBQVEsRUFBRSxHQUFHLENBQUMsUUFBUTt3QkFDdEIsV0FBVyxFQUFFLElBQUk7cUJBQ3BCLENBQUMsQ0FBQztnQkFDUCxDQUFDO2FBQ0osQ0FBQyxDQUFDO1NBQ047SUFDTCxDQUFDO0lBQ0QsV0FBVyxFQUFFLFVBQVUsQ0FBcUU7UUFDeEYsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUNmLEdBQUcsQ0FBQyxVQUFVLENBQUMsUUFBUSxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDO1FBQzVDLElBQUksQ0FBQyxPQUFPLENBQUM7WUFDVCxRQUFRLEVBQUUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxRQUFRO1lBQzNCLFdBQVcsRUFBRSxJQUFJO1NBQ3BCLENBQUMsQ0FBQztJQUNQLENBQUM7SUFDRCxLQUFLO1FBQ0QsRUFBRSxDQUFDLEtBQUssQ0FBQztZQUNQLE9BQU8sRUFBRSxVQUFBLEdBQUc7Z0JBQ2QsRUFBRSxDQUFDLE9BQU8sQ0FBQztvQkFDVCxHQUFHLEVBQUUsd0NBQXdDO29CQUM3QyxJQUFJLEVBQUU7d0JBRUosS0FBSyxFQUFFLG9CQUFvQjt3QkFFM0IsTUFBTSxFQUFFLGtDQUFrQzt3QkFDMUMsSUFBSSxFQUFFLEdBQUcsQ0FBQyxJQUFJO3FCQUNmO29CQUNELE1BQU0sRUFBRSxNQUFNO29CQUNkLE1BQU0sRUFBRSxFQUFFLGNBQWMsRUFBRSxrQkFBa0IsRUFBQztvQkFDN0MsT0FBTyxFQUFFLFVBQVMsU0FBYTt3QkFDM0IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQzt3QkFFckMsSUFBTSxJQUFJLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFBO3dCQUU1QyxJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUU7NEJBQ2YsRUFBRSxDQUFDLGNBQWMsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFBO3lCQUN6Qzs2QkFBTTs0QkFDTCxFQUFFLENBQUMsU0FBUyxDQUFDO2dDQUNYLEtBQUssRUFBRSxJQUFJLENBQUMsTUFBTTtnQ0FDbEIsSUFBSSxFQUFFLE1BQU07Z0NBQ1osUUFBUSxFQUFFLElBQUk7NkJBQ2YsQ0FBQyxDQUFBO3lCQUNIO29CQUVMLENBQUM7b0JBQ0QsSUFBSSxFQUFFLFVBQVMsS0FBSzt3QkFDaEIsT0FBTyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQzt3QkFDN0IsT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztvQkFDeEIsQ0FBQztpQkFDSCxDQUFDLENBQUE7WUFDSCxDQUFDO1NBQ0YsQ0FBQyxDQUFBO0lBQ0YsQ0FBQztDQUNBLENBQUMsQ0FBQSIsInNvdXJjZXNDb250ZW50IjpbImNvbnN0IGFwcCA9IGdldEFwcDxJQXBwT3B0aW9uPigpXHJcblBhZ2Uoe1xyXG4gIGRhdGE6IHtcclxuICAgIG1vdHRvOiAnSGVsbG8gV29ybGQnLFxyXG4gICAgdXNlckluZm86IHt9LFxyXG4gICAgaGFzVXNlckluZm86IGZhbHNlLFxyXG4gICAgY2FuSVVzZTogd3guY2FuSVVzZSgnYnV0dG9uLm9wZW4tdHlwZS5nZXRVc2VySW5mbycpLFxyXG59LFxyXG5iaW5kVmlld1RhcDogZnVuY3Rpb24gKCkge1xyXG4gICAgd3gubmF2aWdhdGVUbyh7XHJcbiAgICAgICAgdXJsOiAnLi4vbG9ncy9sb2dzJyxcclxuICAgIH0pO1xyXG59LFxyXG5vbkxvYWQ6IGZ1bmN0aW9uICgpIHtcclxuICAgIHZhciBfdGhpcyA9IHRoaXM7XHJcbiAgICBpZiAoYXBwLmdsb2JhbERhdGEudXNlckluZm8pIHtcclxuICAgICAgICB0aGlzLnNldERhdGEoe1xyXG4gICAgICAgICAgICB1c2VySW5mbzogYXBwLmdsb2JhbERhdGEudXNlckluZm8sXHJcbiAgICAgICAgICAgIGhhc1VzZXJJbmZvOiB0cnVlLFxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG4gICAgZWxzZSBpZiAodGhpcy5kYXRhLmNhbklVc2UpIHtcclxuICAgICAgICBhcHAudXNlckluZm9SZWFkeUNhbGxiYWNrID0gZnVuY3Rpb24gKHJlcykge1xyXG4gICAgICAgICAgICBfdGhpcy5zZXREYXRhKHtcclxuICAgICAgICAgICAgICAgIHVzZXJJbmZvOiByZXMudXNlckluZm8sXHJcbiAgICAgICAgICAgICAgICBoYXNVc2VySW5mbzogdHJ1ZSxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfTtcclxuICAgIH1cclxuICAgIGVsc2Uge1xyXG4gICAgICAgIHd4LmdldFVzZXJJbmZvKHtcclxuICAgICAgICAgICAgc3VjY2VzczogZnVuY3Rpb24gKHJlcykge1xyXG4gICAgICAgICAgICAgICAgYXBwLmdsb2JhbERhdGEudXNlckluZm8gPSByZXMudXNlckluZm87XHJcbiAgICAgICAgICAgICAgICBfdGhpcy5zZXREYXRhKHtcclxuICAgICAgICAgICAgICAgICAgICB1c2VySW5mbzogcmVzLnVzZXJJbmZvLFxyXG4gICAgICAgICAgICAgICAgICAgIGhhc1VzZXJJbmZvOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcbn0sXHJcbmdldFVzZXJJbmZvOiBmdW5jdGlvbiAoZTogeyBkZXRhaWw6IHsgdXNlckluZm86IFdlY2hhdE1pbmlwcm9ncmFtLlVzZXJJbmZvIHwgdW5kZWZpbmVkOyB9OyB9KSB7XHJcbiAgICBjb25zb2xlLmxvZyhlKTtcclxuICAgIGFwcC5nbG9iYWxEYXRhLnVzZXJJbmZvID0gZS5kZXRhaWwudXNlckluZm87XHJcbiAgICB0aGlzLnNldERhdGEoe1xyXG4gICAgICAgIHVzZXJJbmZvOiBlLmRldGFpbC51c2VySW5mbyxcclxuICAgICAgICBoYXNVc2VySW5mbzogdHJ1ZSxcclxuICAgIH0pO1xyXG59LFxyXG5sb2dpbigpIHtcclxuICAgIHd4LmxvZ2luKHtcclxuICAgICAgc3VjY2VzczogcmVzID0+IHtcclxuICAgIHd4LnJlcXVlc3Qoe1xyXG4gICAgICB1cmw6ICdodHRwOi8vNDcuMTAyLjExNy4yMjQ6ODA4MC9nZXQtb3Blbi1pZCcsXHJcbiAgICAgIGRhdGE6IHtcclxuICAgICAgICAgLy/loavkuIroh6rlt7HnmoTlsI/nqIvluo/llK/kuIDmoIfor4ZcclxuICAgICAgICBhcHBpZDogJ3d4OWI0ZjkwMjIyODY0ZDI1NycsXHJcbiAgICAgICAgIC8v5aGr5LiK6Ieq5bex55qE5bCP56iL5bqP55qEIGFwcCBzZWNyZXRcclxuICAgICAgICBzZWNyZXQ6ICc1YTg1MGNiYzYzNTc0MTgyOTlmOTAyYmU4NmVmZjQwOCcsXHJcbiAgICAgICAgY29kZTogcmVzLmNvZGVcclxuICAgICAgfSxcclxuICAgICAgbWV0aG9kOiAnUE9TVCcsXHJcbiAgICAgIGhlYWRlcjogeyAnY29udGVudC10eXBlJzogJ2FwcGxpY2F0aW9uL2pzb24nfSxcclxuICAgICAgc3VjY2VzczogZnVuY3Rpb24ob3BlbklkUmVzOmFueSl7XHJcbiAgICAgICAgICBjb25zb2xlLmxvZyhyZXMuY29kZSxvcGVuSWRSZXMuZGF0YSk7XHJcblxyXG4gICAgICAgICAgY29uc3QgZGF0YSA9IEpTT04ucGFyc2Uob3BlbklkUmVzLmRhdGEuZGF0YSlcclxuICAgICAgICAgIFxyXG4gICAgICAgICAgaWYgKGRhdGEub3BlbmlkKSB7XHJcbiAgICAgICAgICAgIHd4LnNldFN0b3JhZ2VTeW5jKCdvcGVuaWQnLCBkYXRhLm9wZW5pZClcclxuICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHd4LnNob3dUb2FzdCh7XHJcbiAgICAgICAgICAgICAgdGl0bGU6IGRhdGEuZXJybXNnLFxyXG4gICAgICAgICAgICAgIGljb246ICdub25lJyxcclxuICAgICAgICAgICAgICBkdXJhdGlvbjogMjAwMFxyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgICAgfVxyXG4gICAgICAgICBcclxuICAgICAgfSxcclxuICAgICAgZmFpbDogZnVuY3Rpb24oZXJyb3IpIHtcclxuICAgICAgICAgIGNvbnNvbGUuaW5mbyhcIuiOt+WPlueUqOaIt29wZW5JZOWksei0pVwiKTtcclxuICAgICAgICAgIGNvbnNvbGUuaW5mbyhlcnJvcik7XHJcbiAgICAgIH1cclxuICAgfSlcclxuICB9LFxyXG59KVxyXG59XHJcbn0pIl19