const app = getApp<IAppOption>()
Page({
  data: {
    motto: 'Hello World',
    userInfo: {},
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
},
bindViewTap: function () {
    wx.navigateTo({
        url: '../logs/logs',
    });
},
onLoad: function () {
    var _this = this;
    if (app.globalData.userInfo) {
        this.setData({
            userInfo: app.globalData.userInfo,
            hasUserInfo: true,
        });
    }
    else if (this.data.canIUse) {
        app.userInfoReadyCallback = function (res) {
            _this.setData({
                userInfo: res.userInfo,
                hasUserInfo: true,
            });
        };
    }
    else {
        wx.getUserInfo({
            success: function (res) {
                app.globalData.userInfo = res.userInfo;
                _this.setData({
                    userInfo: res.userInfo,
                    hasUserInfo: true,
                });
            },
        });
    }
},
getUserInfo: function (e: { detail: { userInfo: WechatMiniprogram.UserInfo | undefined; }; }) {
    console.log(e);
    app.globalData.userInfo = e.detail.userInfo;
    this.setData({
        userInfo: e.detail.userInfo,
        hasUserInfo: true,
    });
},
login() {
    wx.login({
      success: res => {
    wx.request({
      url: 'http://47.102.117.224:8080/get-open-id',
      data: {
         //填上自己的小程序唯一标识
        appid: 'wx9b4f90222864d257',
         //填上自己的小程序的 app secret
        secret: '5a850cbc6357418299f902be86eff408',
        code: res.code
      },
      method: 'POST',
      header: { 'content-type': 'application/json'},
      success: function(openIdRes:any){
          console.log(res.code,openIdRes.data);

          const data = JSON.parse(openIdRes.data.data)
          
          if (data.openid) {
            wx.setStorageSync('openid', data.openid)
          } else {
            wx.showToast({
              title: data.errmsg,
              icon: 'none',
              duration: 2000
            })
          }
         
      },
      fail: function(error) {
          console.info("获取用户openId失败");
          console.info(error);
      }
   })
  },
})
}
})