Page({
  data: {
    isShowBar:false,
    isShowMenu: false,
    showLoading:false,
    article:{},
    sections: [],
    book:{},
    scrollTop:0
  },
  onPageScroll:function(e) {
    this.setData({scrollTop: e.scrollTop})
  },
  onLoad: function(e){
   let id = e.id;
   let articleId = e.articleId;
   if(!articleId) {
     const data = wx.getStorageSync('book')
     if(data) {
      const d = data.find(v => parseInt(v.bookId,10) === parseInt(id as string,10))
      console.warn(d,id)
      if(d && d.articleId) {
        articleId = d.articleId
      }
      if(d && d.scrollTop) {
        this.setData({scrollTop:d.scrollTop})
      }
     }
   }


   this.getBook(id)
   this.getSectionList(id,articleId)
  },
  onUnload:function() {
    const bookId = (this.data.book as any).id;
    const articleId = (this.data.article as any).id;
    const scrollTop=this.data.scrollTop;
    let data= wx.getStorageSync('book');
    if(!data) {
      data = [{bookId,articleId,scrollTop}]
    } else {
      const index = data.findIndex(v =>v.bookId ===  bookId);
      if(index>-1) {
        data[index].articleId = articleId
        data[index].scrollTop = scrollTop
      } else {
        data.push( {bookId,articleId,scrollTop})
      }
    }
    
    wx.setStorageSync('book', data)
  },

  getBook:function(id) {
    wx.request({
      method:"GET",
      url: 'http://47.102.117.224:8080/book/'+id,
      header: {
        'content-type': 'application/json' // 默认值
      },
      success:  (res: {data: any}) => {
        this.setData({book:{...res.data.data,update_time: this.formatTime(res.data.data.update_time)}})
      }
    })
  },

  getSectionList :function(id,articleId) {
    this.setData({showLoading:true})
    wx.request({
      method:"GET",
      url: 'http://47.102.117.224:8080/article-section/'+id,
      header: {
        'content-type': 'application/json' // 默认值
      },
      success:  (res: {data: any}) => {
        this.setData({sections:res.data.data.data})
        if (!!articleId) {
          this.getArticle(articleId)
         } else {
          let _id = res.data.data.data[0].id
          this.getArticle(_id)
         }
      }
    })
  },

  getArticle: function(id) {
    wx.request({
      method:"GET",
      url: 'http://47.102.117.224:8080/article/'+id,
      header: {
        'content-type': 'application/json' // 默认值
      },
      success:  (res: {data: any}) => {
        const text = res.data.data.text.replace(/(\n)+/g,"").replace(/(<br\/>)+/g,"\n").trim()
        this.setData({article:{...res.data.data,text},showLoading:false})
        setTimeout(()=> {
          wx.pageScrollTo({
          scrollTop: this.data.scrollTop
        })
      },100)
      }
    })
  },

  goToPre:function() {
    const index = this.data.sections.findIndex((v)=> {return v.id === (this.data.article as any).id })
    if (index===0) {
      wx.showToast({
        title: '已到达第一章',
        icon: 'none',
        duration: 2000
      })
      this.setData({showToast:'pre'})
      return;
    }
    const data:any = this.data.sections[index-1]
    wx.navigateTo({url:`/pages/detail/index?id=${data.book_id}&articleId=${data.id}`})
  },
  goToNext:function() {
    const index = this.data.sections.findIndex((v)=> {return v.id === (this.data.article as any).id })
    if (index===this.data.sections.length-1) {
      wx.showToast({
        title: '已到达最后一章',
        icon: 'none',
        duration: 2000
      })
      return;
    }
    const data:any = this.data.sections[index+1]
    wx.navigateTo({url:`/pages/detail/index?id=${data.book_id}&articleId=${data.id}`})
  },
  gotoHome:function() {
    wx.switchTab({url:`/pages/index/index`})
  },
  midaction: function(e){
    if(e.detail.y > 400 ) {
      this.setData({isShowBar: !this.data.isShowBar})
    } 
  },
  showMenu:function() {
     this.setData({isShowMenu:true,isShowBar:false})
  },
  hideMenu:function() {
     this.setData({isShowMenu:false})
  },
  inbtn:function() {
    console.log("阻止冒泡");
  },
  formatTime:(date) => {
    var year = new Date(date).getFullYear()
    var month = new Date(date).getMonth() + 1
    var day = new Date(date).getDate()
    var hour = new Date(date).getHours()
    var minute = new Date(date).getMinutes()
    var second = new Date(date).getSeconds()
    return [year, month, day].join('-') + ' ' + [hour, minute, second].join(':')
   },
   toastChange: function() {
     this.setData({showToast:""})
   }
  
})