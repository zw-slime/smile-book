// index.ts
Page({
  data: {
    bookList: [],
    bookType:[],
    currentTypeId:0
  },
  onLoad: function() {
    console.warn(1111)
    this.getAllType();
  },
  getAllType: function() {
    console.warn(1111)
    wx.request({
      method:"GET",
      url: 'http://47.102.117.224:8080/book-type', //仅为示例，并非真实的接口地址
      header: {
        'content-type': 'application/json' // 默认值
      },
      success:  (res: {data: any}) => {
        console.warn(res.data.data)
        this.setData({bookType:res.data.data.map(v => ({...v,name: v.name.slice(0,2)})),currentTypeId:(res.data.data[0] as any).id })
        this.getBookList(this.data.currentTypeId)
      }
    })
  },
  getBookList: function(typeId:number) {
    wx.request({
      method:"POST",
      url: 'http://47.102.117.224:8080/book-list', //仅为示例，并非真实的接口地址
      header: {
        'content-type': 'application/json' // 默认值
      },
      data: {
        "type_id":typeId
      },
      success:  (res: {data: any}) => {
        this.setData({bookList:res.data.data})
      }
    })
  },
  setType: function(e) {
    this.setData({currentTypeId: e.currentTarget.dataset.id})
    this.getBookList(this.data.currentTypeId)
  }
})
