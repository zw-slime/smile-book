"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Page({
    data: {
        bookList: [],
        bookType: [],
        currentTypeId: 0
    },
    onLoad: function () {
        console.warn(1111);
        this.getAllType();
    },
    getAllType: function () {
        var _this = this;
        console.warn(1111);
        wx.request({
            method: "GET",
            url: 'http://47.102.117.224:8080/book-type',
            header: {
                'content-type': 'application/json'
            },
            success: function (res) {
                console.warn(res.data.data);
                _this.setData({ bookType: res.data.data.map(function (v) { return (__assign({}, v, { name: v.name.slice(0, 2) })); }), currentTypeId: res.data.data[0].id });
                _this.getBookList(_this.data.currentTypeId);
            }
        });
    },
    getBookList: function (typeId) {
        var _this = this;
        wx.request({
            method: "POST",
            url: 'http://47.102.117.224:8080/book-list',
            header: {
                'content-type': 'application/json'
            },
            data: {
                "type_id": typeId
            },
            success: function (res) {
                _this.setData({ bookList: res.data.data });
            }
        });
    },
    setType: function (e) {
        this.setData({ currentTypeId: e.currentTarget.dataset.id });
        this.getBookList(this.data.currentTypeId);
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7QUFDQSxJQUFJLENBQUM7SUFDSCxJQUFJLEVBQUU7UUFDSixRQUFRLEVBQUUsRUFBRTtRQUNaLFFBQVEsRUFBQyxFQUFFO1FBQ1gsYUFBYSxFQUFDLENBQUM7S0FDaEI7SUFDRCxNQUFNLEVBQUU7UUFDTixPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFBO1FBQ2xCLElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQztJQUNwQixDQUFDO0lBQ0QsVUFBVSxFQUFFO1FBQUEsaUJBY1g7UUFiQyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFBO1FBQ2xCLEVBQUUsQ0FBQyxPQUFPLENBQUM7WUFDVCxNQUFNLEVBQUMsS0FBSztZQUNaLEdBQUcsRUFBRSxzQ0FBc0M7WUFDM0MsTUFBTSxFQUFFO2dCQUNOLGNBQWMsRUFBRSxrQkFBa0I7YUFDbkM7WUFDRCxPQUFPLEVBQUcsVUFBQyxHQUFnQjtnQkFDekIsT0FBTyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFBO2dCQUMzQixLQUFJLENBQUMsT0FBTyxDQUFDLEVBQUMsUUFBUSxFQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxVQUFBLENBQUMsSUFBSSxPQUFBLGNBQUssQ0FBQyxJQUFDLElBQUksRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLElBQUUsRUFBaEMsQ0FBZ0MsQ0FBQyxFQUFDLGFBQWEsRUFBRSxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQVMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFBO2dCQUM3SCxLQUFJLENBQUMsV0FBVyxDQUFDLEtBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUE7WUFDM0MsQ0FBQztTQUNGLENBQUMsQ0FBQTtJQUNKLENBQUM7SUFDRCxXQUFXLEVBQUUsVUFBUyxNQUFhO1FBQXRCLGlCQWNaO1FBYkMsRUFBRSxDQUFDLE9BQU8sQ0FBQztZQUNULE1BQU0sRUFBQyxNQUFNO1lBQ2IsR0FBRyxFQUFFLHNDQUFzQztZQUMzQyxNQUFNLEVBQUU7Z0JBQ04sY0FBYyxFQUFFLGtCQUFrQjthQUNuQztZQUNELElBQUksRUFBRTtnQkFDSixTQUFTLEVBQUMsTUFBTTthQUNqQjtZQUNELE9BQU8sRUFBRyxVQUFDLEdBQWdCO2dCQUN6QixLQUFJLENBQUMsT0FBTyxDQUFDLEVBQUMsUUFBUSxFQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFDLENBQUMsQ0FBQTtZQUN4QyxDQUFDO1NBQ0YsQ0FBQyxDQUFBO0lBQ0osQ0FBQztJQUNELE9BQU8sRUFBRSxVQUFTLENBQUM7UUFDakIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxFQUFDLGFBQWEsRUFBRSxDQUFDLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxFQUFFLEVBQUMsQ0FBQyxDQUFBO1FBQ3pELElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQTtJQUMzQyxDQUFDO0NBQ0YsQ0FBQyxDQUFBIiwic291cmNlc0NvbnRlbnQiOlsiLy8gaW5kZXgudHNcclxuUGFnZSh7XHJcbiAgZGF0YToge1xyXG4gICAgYm9va0xpc3Q6IFtdLFxyXG4gICAgYm9va1R5cGU6W10sXHJcbiAgICBjdXJyZW50VHlwZUlkOjBcclxuICB9LFxyXG4gIG9uTG9hZDogZnVuY3Rpb24oKSB7XHJcbiAgICBjb25zb2xlLndhcm4oMTExMSlcclxuICAgIHRoaXMuZ2V0QWxsVHlwZSgpO1xyXG4gIH0sXHJcbiAgZ2V0QWxsVHlwZTogZnVuY3Rpb24oKSB7XHJcbiAgICBjb25zb2xlLndhcm4oMTExMSlcclxuICAgIHd4LnJlcXVlc3Qoe1xyXG4gICAgICBtZXRob2Q6XCJHRVRcIixcclxuICAgICAgdXJsOiAnaHR0cDovLzQ3LjEwMi4xMTcuMjI0OjgwODAvYm9vay10eXBlJywgLy/ku4XkuLrnpLrkvovvvIzlubbpnZ7nnJ/lrp7nmoTmjqXlj6PlnLDlnYBcclxuICAgICAgaGVhZGVyOiB7XHJcbiAgICAgICAgJ2NvbnRlbnQtdHlwZSc6ICdhcHBsaWNhdGlvbi9qc29uJyAvLyDpu5jorqTlgLxcclxuICAgICAgfSxcclxuICAgICAgc3VjY2VzczogIChyZXM6IHtkYXRhOiBhbnl9KSA9PiB7XHJcbiAgICAgICAgY29uc29sZS53YXJuKHJlcy5kYXRhLmRhdGEpXHJcbiAgICAgICAgdGhpcy5zZXREYXRhKHtib29rVHlwZTpyZXMuZGF0YS5kYXRhLm1hcCh2ID0+ICh7Li4udixuYW1lOiB2Lm5hbWUuc2xpY2UoMCwyKX0pKSxjdXJyZW50VHlwZUlkOihyZXMuZGF0YS5kYXRhWzBdIGFzIGFueSkuaWQgfSlcclxuICAgICAgICB0aGlzLmdldEJvb2tMaXN0KHRoaXMuZGF0YS5jdXJyZW50VHlwZUlkKVxyXG4gICAgICB9XHJcbiAgICB9KVxyXG4gIH0sXHJcbiAgZ2V0Qm9va0xpc3Q6IGZ1bmN0aW9uKHR5cGVJZDpudW1iZXIpIHtcclxuICAgIHd4LnJlcXVlc3Qoe1xyXG4gICAgICBtZXRob2Q6XCJQT1NUXCIsXHJcbiAgICAgIHVybDogJ2h0dHA6Ly80Ny4xMDIuMTE3LjIyNDo4MDgwL2Jvb2stbGlzdCcsIC8v5LuF5Li656S65L6L77yM5bm26Z2e55yf5a6e55qE5o6l5Y+j5Zyw5Z2AXHJcbiAgICAgIGhlYWRlcjoge1xyXG4gICAgICAgICdjb250ZW50LXR5cGUnOiAnYXBwbGljYXRpb24vanNvbicgLy8g6buY6K6k5YC8XHJcbiAgICAgIH0sXHJcbiAgICAgIGRhdGE6IHtcclxuICAgICAgICBcInR5cGVfaWRcIjp0eXBlSWRcclxuICAgICAgfSxcclxuICAgICAgc3VjY2VzczogIChyZXM6IHtkYXRhOiBhbnl9KSA9PiB7XHJcbiAgICAgICAgdGhpcy5zZXREYXRhKHtib29rTGlzdDpyZXMuZGF0YS5kYXRhfSlcclxuICAgICAgfVxyXG4gICAgfSlcclxuICB9LFxyXG4gIHNldFR5cGU6IGZ1bmN0aW9uKGUpIHtcclxuICAgIHRoaXMuc2V0RGF0YSh7Y3VycmVudFR5cGVJZDogZS5jdXJyZW50VGFyZ2V0LmRhdGFzZXQuaWR9KVxyXG4gICAgdGhpcy5nZXRCb29rTGlzdCh0aGlzLmRhdGEuY3VycmVudFR5cGVJZClcclxuICB9XHJcbn0pXHJcbiJdfQ==