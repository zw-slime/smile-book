"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Page({
    data: {
        book: {},
        sections: [],
        showSection: false,
        bookList: [],
        isAdded: false
    },
    onLoad: function (e) {
        var _this = this;
        wx.request({
            method: "GET",
            url: 'http://47.102.117.224:8080/book-detail/' + e.id,
            header: {
                'content-type': 'application/json'
            },
            success: function (res) {
                _this.setData({ book: __assign({}, res.data.data, { update_time: _this.formatTime(res.data.data.update_time) }) });
                _this.getList();
            }
        });
        this.getSectionList(e.id);
    },
    formatTime: function (date) {
        var year = new Date(date).getFullYear();
        var month = new Date(date).getMonth() + 1;
        var day = new Date(date).getDate();
        var hour = new Date(date).getHours();
        var minute = new Date(date).getMinutes();
        var second = new Date(date).getSeconds();
        return [year, month, day].join('-') + ' ' + [hour, minute, second].join(':');
    },
    toggleSection: function (e) {
        this.setData({ showSection: e.currentTarget.dataset.show });
    },
    getSectionList: function (id) {
        var _this = this;
        this.setData({ showLoading: true });
        wx.request({
            method: "GET",
            url: 'http://47.102.117.224:8080/article-section/' + id,
            header: {
                'content-type': 'application/json'
            },
            success: function (res) {
                _this.setData({ sections: res.data.data.data });
            }
        });
    },
    getList: function () {
        var _this = this;
        var openid = wx.getStorageSync('openid');
        wx.request({
            method: "POST",
            url: 'http://47.102.117.224:8080/user-book',
            header: {
                'content-type': 'application/json'
            },
            data: { openid: openid },
            success: function (res) {
                if (!res.data.data || res.data.data.length <= 0) {
                    _this.setData({ bookList: [], recommend: {} });
                }
                else {
                    _this.setData({ bookList: res.data.data, recommend: res.data.data[res.data.data.length - 1] });
                }
                if (_this.data.bookList.findIndex(function (v) { return v.book_id === _this.data.book.id; }) > -1) {
                    _this.setData({ isAdded: true });
                }
                else {
                    _this.setData({ isAdded: false });
                }
            }
        });
    },
    addBook: function () {
        var _this = this;
        var openid = wx.getStorageSync('openid');
        wx.request({
            method: "POST",
            url: 'http://47.102.117.224:8080/add-user-book',
            header: {
                'content-type': 'application/json'
            },
            data: {
                book_id: this.data.book.id,
                openid: openid
            },
            success: function (res) {
                if (res.data.meta.err_code !== 0) {
                    wx.showToast({
                        title: '添加到书架失败，失败原因：' + res.data.meta.err_msg,
                        icon: 'none',
                        duration: 2000
                    });
                }
                else {
                    wx.showToast({
                        title: '成功添加到书架',
                        icon: 'success',
                        duration: 2000
                    });
                    _this.getList();
                }
            }
        });
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7QUFBQSxJQUFJLENBQUM7SUFDSCxJQUFJLEVBQUU7UUFDSixJQUFJLEVBQUMsRUFBRTtRQUNQLFFBQVEsRUFBQyxFQUFFO1FBQ1gsV0FBVyxFQUFFLEtBQUs7UUFDbEIsUUFBUSxFQUFDLEVBQUU7UUFDWCxPQUFPLEVBQUUsS0FBSztLQUNmO0lBQ0QsTUFBTSxZQUFDLENBQUM7UUFBUixpQkFhQztRQVpDLEVBQUUsQ0FBQyxPQUFPLENBQUM7WUFDVCxNQUFNLEVBQUMsS0FBSztZQUNaLEdBQUcsRUFBRSx5Q0FBeUMsR0FBQyxDQUFDLENBQUMsRUFBRTtZQUNuRCxNQUFNLEVBQUU7Z0JBQ04sY0FBYyxFQUFFLGtCQUFrQjthQUNuQztZQUNELE9BQU8sRUFBRyxVQUFDLEdBQWdCO2dCQUN6QixLQUFJLENBQUMsT0FBTyxDQUFDLEVBQUMsSUFBSSxlQUFLLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxJQUFDLFdBQVcsRUFBRSxLQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFDLEVBQUMsQ0FBQyxDQUFDO2dCQUNoRyxLQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7WUFDakIsQ0FBQztTQUNGLENBQUMsQ0FBQTtRQUNGLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDO0lBQzVCLENBQUM7SUFDRCxVQUFVLEVBQUMsVUFBQyxJQUFJO1FBQ2QsSUFBSSxJQUFJLEdBQUcsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsV0FBVyxFQUFFLENBQUE7UUFDdkMsSUFBSSxLQUFLLEdBQUcsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsUUFBUSxFQUFFLEdBQUcsQ0FBQyxDQUFBO1FBQ3pDLElBQUksR0FBRyxHQUFHLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLE9BQU8sRUFBRSxDQUFBO1FBQ2xDLElBQUksSUFBSSxHQUFHLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLFFBQVEsRUFBRSxDQUFBO1FBQ3BDLElBQUksTUFBTSxHQUFHLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLFVBQVUsRUFBRSxDQUFBO1FBQ3hDLElBQUksTUFBTSxHQUFHLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLFVBQVUsRUFBRSxDQUFBO1FBQ3hDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsS0FBSyxFQUFFLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxHQUFHLEdBQUcsQ0FBQyxJQUFJLEVBQUUsTUFBTSxFQUFFLE1BQU0sQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQTtJQUM3RSxDQUFDO0lBQ0QsYUFBYSxFQUFFLFVBQVMsQ0FBQztRQUN4QixJQUFJLENBQUMsT0FBTyxDQUFDLEVBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLElBQUksRUFBQyxDQUFDLENBQUE7SUFDM0QsQ0FBQztJQUNBLGNBQWMsRUFBRSxVQUFTLEVBQUU7UUFBWCxpQkFZaEI7UUFYQyxJQUFJLENBQUMsT0FBTyxDQUFDLEVBQUMsV0FBVyxFQUFDLElBQUksRUFBQyxDQUFDLENBQUE7UUFDaEMsRUFBRSxDQUFDLE9BQU8sQ0FBQztZQUNULE1BQU0sRUFBQyxLQUFLO1lBQ1osR0FBRyxFQUFFLDZDQUE2QyxHQUFDLEVBQUU7WUFDckQsTUFBTSxFQUFFO2dCQUNOLGNBQWMsRUFBRSxrQkFBa0I7YUFDbkM7WUFDRCxPQUFPLEVBQUcsVUFBQyxHQUFnQjtnQkFDekIsS0FBSSxDQUFDLE9BQU8sQ0FBQyxFQUFDLFFBQVEsRUFBQyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUMsQ0FBQyxDQUFBO1lBQzdDLENBQUM7U0FDRixDQUFDLENBQUE7SUFDSixDQUFDO0lBQ0QsT0FBTztRQUFQLGlCQXVCQztRQXRCQyxJQUFJLE1BQU0sR0FBRyxFQUFFLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxDQUFBO1FBQ3hDLEVBQUUsQ0FBQyxPQUFPLENBQUM7WUFDVCxNQUFNLEVBQUMsTUFBTTtZQUNiLEdBQUcsRUFBRSxzQ0FBc0M7WUFDM0MsTUFBTSxFQUFFO2dCQUNOLGNBQWMsRUFBRSxrQkFBa0I7YUFDbkM7WUFDRCxJQUFJLEVBQUUsRUFBQyxNQUFNLFFBQUEsRUFBQztZQUNkLE9BQU8sRUFBRyxVQUFDLEdBQWdCO2dCQUN6QixJQUFHLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLElBQUksR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxJQUFHLENBQUMsRUFBRTtvQkFDN0MsS0FBSSxDQUFDLE9BQU8sQ0FBQyxFQUFDLFFBQVEsRUFBQyxFQUFFLEVBQUMsU0FBUyxFQUFDLEVBQUUsRUFBQyxDQUFDLENBQUE7aUJBQ3pDO3FCQUFNO29CQUNMLEtBQUksQ0FBQyxPQUFPLENBQUMsRUFBQyxRQUFRLEVBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUMsU0FBUyxFQUFFLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBQyxDQUFDLENBQUMsRUFBQyxDQUFDLENBQUE7aUJBQ3hGO2dCQUVELElBQUcsS0FBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxDQUFDLE9BQU8sS0FBTSxLQUFJLENBQUMsSUFBSSxDQUFDLElBQVksQ0FBQyxFQUFFLEVBQXhDLENBQXdDLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRTtvQkFDbkYsS0FBSSxDQUFDLE9BQU8sQ0FBQyxFQUFDLE9BQU8sRUFBQyxJQUFJLEVBQUMsQ0FBQyxDQUFBO2lCQUM3QjtxQkFBTTtvQkFDTCxLQUFJLENBQUMsT0FBTyxDQUFDLEVBQUMsT0FBTyxFQUFDLEtBQUssRUFBQyxDQUFDLENBQUE7aUJBQzlCO1lBQ0gsQ0FBQztTQUNGLENBQUMsQ0FBQTtJQUNKLENBQUM7SUFDRCxPQUFPLEVBQUM7UUFBQSxpQkE2QlA7UUE1QkMsSUFBSSxNQUFNLEdBQUcsRUFBRSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsQ0FBQTtRQUN4QyxFQUFFLENBQUMsT0FBTyxDQUFDO1lBQ1QsTUFBTSxFQUFDLE1BQU07WUFDYixHQUFHLEVBQUUsMENBQTBDO1lBQy9DLE1BQU0sRUFBRTtnQkFDTixjQUFjLEVBQUUsa0JBQWtCO2FBQ25DO1lBQ0QsSUFBSSxFQUFFO2dCQUNKLE9BQU8sRUFBRyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQVksQ0FBQyxFQUFFO2dCQUNuQyxNQUFNLFFBQUE7YUFDUDtZQUNELE9BQU8sRUFBRyxVQUFDLEdBQWdCO2dCQUN6QixJQUFJLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsS0FBSyxDQUFDLEVBQUU7b0JBQ2hDLEVBQUUsQ0FBQyxTQUFTLENBQUM7d0JBQ1gsS0FBSyxFQUFFLGVBQWUsR0FBRSxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPO3dCQUM3QyxJQUFJLEVBQUUsTUFBTTt3QkFDWixRQUFRLEVBQUUsSUFBSTtxQkFDZixDQUFDLENBQUE7aUJBQ0g7cUJBQU07b0JBQ0wsRUFBRSxDQUFDLFNBQVMsQ0FBQzt3QkFDWCxLQUFLLEVBQUUsU0FBUzt3QkFDaEIsSUFBSSxFQUFFLFNBQVM7d0JBQ2YsUUFBUSxFQUFFLElBQUk7cUJBQ2YsQ0FBQyxDQUFBO29CQUNGLEtBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztpQkFDaEI7WUFDSCxDQUFDO1NBQ0YsQ0FBQyxDQUFBO0lBQ0osQ0FBQztDQUVGLENBQUMsQ0FBQSIsInNvdXJjZXNDb250ZW50IjpbIlBhZ2Uoe1xyXG4gIGRhdGE6IHtcclxuICAgIGJvb2s6e30sXHJcbiAgICBzZWN0aW9uczpbXSxcclxuICAgIHNob3dTZWN0aW9uOiBmYWxzZSxcclxuICAgIGJvb2tMaXN0OltdLFxyXG4gICAgaXNBZGRlZDogZmFsc2VcclxuICB9LFxyXG4gIG9uTG9hZChlKSB7XHJcbiAgICB3eC5yZXF1ZXN0KHtcclxuICAgICAgbWV0aG9kOlwiR0VUXCIsXHJcbiAgICAgIHVybDogJ2h0dHA6Ly80Ny4xMDIuMTE3LjIyNDo4MDgwL2Jvb2stZGV0YWlsLycrZS5pZCxcclxuICAgICAgaGVhZGVyOiB7XHJcbiAgICAgICAgJ2NvbnRlbnQtdHlwZSc6ICdhcHBsaWNhdGlvbi9qc29uJyAvLyDpu5jorqTlgLxcclxuICAgICAgfSxcclxuICAgICAgc3VjY2VzczogIChyZXM6IHtkYXRhOiBhbnl9KSA9PiB7XHJcbiAgICAgICAgdGhpcy5zZXREYXRhKHtib29rOnsuLi5yZXMuZGF0YS5kYXRhLHVwZGF0ZV90aW1lOiB0aGlzLmZvcm1hdFRpbWUocmVzLmRhdGEuZGF0YS51cGRhdGVfdGltZSl9fSk7XHJcbiAgICAgICAgdGhpcy5nZXRMaXN0KCk7XHJcbiAgICAgIH1cclxuICAgIH0pXHJcbiAgICB0aGlzLmdldFNlY3Rpb25MaXN0KGUuaWQpO1xyXG4gIH0sXHJcbiAgZm9ybWF0VGltZTooZGF0ZSkgPT4ge1xyXG4gICAgdmFyIHllYXIgPSBuZXcgRGF0ZShkYXRlKS5nZXRGdWxsWWVhcigpXHJcbiAgICB2YXIgbW9udGggPSBuZXcgRGF0ZShkYXRlKS5nZXRNb250aCgpICsgMVxyXG4gICAgdmFyIGRheSA9IG5ldyBEYXRlKGRhdGUpLmdldERhdGUoKVxyXG4gICAgdmFyIGhvdXIgPSBuZXcgRGF0ZShkYXRlKS5nZXRIb3VycygpXHJcbiAgICB2YXIgbWludXRlID0gbmV3IERhdGUoZGF0ZSkuZ2V0TWludXRlcygpXHJcbiAgICB2YXIgc2Vjb25kID0gbmV3IERhdGUoZGF0ZSkuZ2V0U2Vjb25kcygpXHJcbiAgICByZXR1cm4gW3llYXIsIG1vbnRoLCBkYXldLmpvaW4oJy0nKSArICcgJyArIFtob3VyLCBtaW51dGUsIHNlY29uZF0uam9pbignOicpXHJcbiAgIH0sXHJcbiAgIHRvZ2dsZVNlY3Rpb246IGZ1bmN0aW9uKGUpIHtcclxuICAgIHRoaXMuc2V0RGF0YSh7c2hvd1NlY3Rpb246IGUuY3VycmVudFRhcmdldC5kYXRhc2V0LnNob3d9KVxyXG4gIH0sXHJcbiAgIGdldFNlY3Rpb25MaXN0IDpmdW5jdGlvbihpZCkge1xyXG4gICAgdGhpcy5zZXREYXRhKHtzaG93TG9hZGluZzp0cnVlfSlcclxuICAgIHd4LnJlcXVlc3Qoe1xyXG4gICAgICBtZXRob2Q6XCJHRVRcIixcclxuICAgICAgdXJsOiAnaHR0cDovLzQ3LjEwMi4xMTcuMjI0OjgwODAvYXJ0aWNsZS1zZWN0aW9uLycraWQsXHJcbiAgICAgIGhlYWRlcjoge1xyXG4gICAgICAgICdjb250ZW50LXR5cGUnOiAnYXBwbGljYXRpb24vanNvbicgLy8g6buY6K6k5YC8XHJcbiAgICAgIH0sXHJcbiAgICAgIHN1Y2Nlc3M6ICAocmVzOiB7ZGF0YTogYW55fSkgPT4ge1xyXG4gICAgICAgIHRoaXMuc2V0RGF0YSh7c2VjdGlvbnM6cmVzLmRhdGEuZGF0YS5kYXRhfSlcclxuICAgICAgfVxyXG4gICAgfSlcclxuICB9LFxyXG4gIGdldExpc3QgKCkge1xyXG4gICAgdmFyIG9wZW5pZCA9IHd4LmdldFN0b3JhZ2VTeW5jKCdvcGVuaWQnKVxyXG4gICAgd3gucmVxdWVzdCh7XHJcbiAgICAgIG1ldGhvZDpcIlBPU1RcIixcclxuICAgICAgdXJsOiAnaHR0cDovLzQ3LjEwMi4xMTcuMjI0OjgwODAvdXNlci1ib29rJywgLy/ku4XkuLrnpLrkvovvvIzlubbpnZ7nnJ/lrp7nmoTmjqXlj6PlnLDlnYBcclxuICAgICAgaGVhZGVyOiB7XHJcbiAgICAgICAgJ2NvbnRlbnQtdHlwZSc6ICdhcHBsaWNhdGlvbi9qc29uJyAvLyDpu5jorqTlgLxcclxuICAgICAgfSxcclxuICAgICAgZGF0YToge29wZW5pZH0sXHJcbiAgICAgIHN1Y2Nlc3M6ICAocmVzOiB7ZGF0YTogYW55fSkgPT4ge1xyXG4gICAgICAgIGlmKCFyZXMuZGF0YS5kYXRhIHx8IHJlcy5kYXRhLmRhdGEubGVuZ3RoIDw9MCkge1xyXG4gICAgICAgICAgdGhpcy5zZXREYXRhKHtib29rTGlzdDpbXSxyZWNvbW1lbmQ6e319KVxyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICB0aGlzLnNldERhdGEoe2Jvb2tMaXN0OnJlcy5kYXRhLmRhdGEscmVjb21tZW5kOiByZXMuZGF0YS5kYXRhW3Jlcy5kYXRhLmRhdGEubGVuZ3RoLTFdfSlcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmKHRoaXMuZGF0YS5ib29rTGlzdC5maW5kSW5kZXgodiA9PiB2LmJvb2tfaWQgPT09ICh0aGlzLmRhdGEuYm9vayBhcyBhbnkpLmlkKSA+IC0xKSB7XHJcbiAgICAgICAgICB0aGlzLnNldERhdGEoe2lzQWRkZWQ6dHJ1ZX0pXHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIHRoaXMuc2V0RGF0YSh7aXNBZGRlZDpmYWxzZX0pXHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9KVxyXG4gIH0sXHJcbiAgYWRkQm9vazpmdW5jdGlvbigpIHtcclxuICAgIHZhciBvcGVuaWQgPSB3eC5nZXRTdG9yYWdlU3luYygnb3BlbmlkJylcclxuICAgIHd4LnJlcXVlc3Qoe1xyXG4gICAgICBtZXRob2Q6XCJQT1NUXCIsXHJcbiAgICAgIHVybDogJ2h0dHA6Ly80Ny4xMDIuMTE3LjIyNDo4MDgwL2FkZC11c2VyLWJvb2snLFxyXG4gICAgICBoZWFkZXI6IHtcclxuICAgICAgICAnY29udGVudC10eXBlJzogJ2FwcGxpY2F0aW9uL2pzb24nIC8vIOm7mOiupOWAvFxyXG4gICAgICB9LFxyXG4gICAgICBkYXRhOiB7XHJcbiAgICAgICAgYm9va19pZDogKHRoaXMuZGF0YS5ib29rIGFzIGFueSkuaWQsXHJcbiAgICAgICAgb3BlbmlkXHJcbiAgICAgIH0sXHJcbiAgICAgIHN1Y2Nlc3M6ICAocmVzOiB7ZGF0YTogYW55fSkgPT4ge1xyXG4gICAgICAgIGlmIChyZXMuZGF0YS5tZXRhLmVycl9jb2RlICE9PSAwKSB7XHJcbiAgICAgICAgICB3eC5zaG93VG9hc3Qoe1xyXG4gICAgICAgICAgICB0aXRsZTogJ+a3u+WKoOWIsOS5puaetuWksei0pe+8jOWksei0peWOn+WboO+8micrIHJlcy5kYXRhLm1ldGEuZXJyX21zZyxcclxuICAgICAgICAgICAgaWNvbjogJ25vbmUnLFxyXG4gICAgICAgICAgICBkdXJhdGlvbjogMjAwMFxyXG4gICAgICAgICAgfSlcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgd3guc2hvd1RvYXN0KHtcclxuICAgICAgICAgICAgdGl0bGU6ICfmiJDlip/mt7vliqDliLDkuabmnrYnLFxyXG4gICAgICAgICAgICBpY29uOiAnc3VjY2VzcycsXHJcbiAgICAgICAgICAgIGR1cmF0aW9uOiAyMDAwXHJcbiAgICAgICAgICB9KVxyXG4gICAgICAgICAgdGhpcy5nZXRMaXN0KCk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9KVxyXG4gIH1cclxuICBcclxufSkiXX0=