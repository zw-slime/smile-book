Page({
  data: {
    book:{},
    sections:[],
    showSection: false,
    bookList:[],
    isAdded: false
  },
  onLoad(e) {
    wx.request({
      method:"GET",
      url: 'http://47.102.117.224:8080/book-detail/'+e.id,
      header: {
        'content-type': 'application/json' // 默认值
      },
      success:  (res: {data: any}) => {
        this.setData({book:{...res.data.data,update_time: this.formatTime(res.data.data.update_time)}});
        this.getList();
      }
    })
    this.getSectionList(e.id);
  },
  formatTime:(date) => {
    var year = new Date(date).getFullYear()
    var month = new Date(date).getMonth() + 1
    var day = new Date(date).getDate()
    var hour = new Date(date).getHours()
    var minute = new Date(date).getMinutes()
    var second = new Date(date).getSeconds()
    return [year, month, day].join('-') + ' ' + [hour, minute, second].join(':')
   },
   toggleSection: function(e) {
    this.setData({showSection: e.currentTarget.dataset.show})
  },
   getSectionList :function(id) {
    this.setData({showLoading:true})
    wx.request({
      method:"GET",
      url: 'http://47.102.117.224:8080/article-section/'+id,
      header: {
        'content-type': 'application/json' // 默认值
      },
      success:  (res: {data: any}) => {
        this.setData({sections:res.data.data.data})
      }
    })
  },
  getList () {
    var openid = wx.getStorageSync('openid')
    wx.request({
      method:"POST",
      url: 'http://47.102.117.224:8080/user-book', //仅为示例，并非真实的接口地址
      header: {
        'content-type': 'application/json' // 默认值
      },
      data: {openid},
      success:  (res: {data: any}) => {
        if(!res.data.data || res.data.data.length <=0) {
          this.setData({bookList:[],recommend:{}})
        } else {
          this.setData({bookList:res.data.data,recommend: res.data.data[res.data.data.length-1]})
        }

        if(this.data.bookList.findIndex(v => v.book_id === (this.data.book as any).id) > -1) {
          this.setData({isAdded:true})
        } else {
          this.setData({isAdded:false})
        }
      }
    })
  },
  addBook:function() {
    var openid = wx.getStorageSync('openid')
    wx.request({
      method:"POST",
      url: 'http://47.102.117.224:8080/add-user-book',
      header: {
        'content-type': 'application/json' // 默认值
      },
      data: {
        book_id: (this.data.book as any).id,
        openid
      },
      success:  (res: {data: any}) => {
        if (res.data.meta.err_code !== 0) {
          wx.showToast({
            title: '添加到书架失败，失败原因：'+ res.data.meta.err_msg,
            icon: 'none',
            duration: 2000
          })
        } else {
          wx.showToast({
            title: '成功添加到书架',
            icon: 'success',
            duration: 2000
          })
          this.getList();
        }
      }
    })
  }
  
})