"use strict";
App({
    globalData: {},
    onLaunch: function () {
        var _this = this;
        wx.login({
            success: function (res) {
                wx.request({
                    url: 'http://47.102.117.224:8080/get-open-id',
                    data: {
                        appid: 'wx9b4f90222864d257',
                        secret: '5a850cbc6357418299f902be86eff408',
                        code: res.code
                    },
                    method: 'POST',
                    header: { 'content-type': 'application/json' },
                    success: function (openIdRes) {
                        console.log(res.code, openIdRes.data);
                        var data = JSON.parse(openIdRes.data.data);
                        if (data.openid) {
                            wx.setStorageSync('openid', data.openid);
                        }
                        else {
                            wx.showToast({
                                title: data.errmsg,
                                icon: 'none',
                                duration: 2000
                            });
                        }
                    },
                    fail: function (error) {
                        console.info("获取用户openId失败");
                        console.info(error);
                    }
                });
            },
        });
        wx.getSetting({
            success: function (res) {
                if (res.authSetting['scope.userInfo']) {
                    wx.getUserInfo({
                        success: function (res) {
                            _this.globalData.userInfo = res.userInfo;
                            if (_this.userInfoReadyCallback) {
                                _this.userInfoReadyCallback(res);
                            }
                        },
                    });
                }
            },
        });
    },
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiYXBwLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFDQSxHQUFHLENBQWE7SUFDZCxVQUFVLEVBQUUsRUFBRTtJQUNkLFFBQVE7UUFBUixpQkErREM7UUF4REMsRUFBRSxDQUFDLEtBQUssQ0FBQztZQUNQLE9BQU8sRUFBRSxVQUFBLEdBQUc7Z0JBQ2QsRUFBRSxDQUFDLE9BQU8sQ0FBQztvQkFDVCxHQUFHLEVBQUUsd0NBQXdDO29CQUM3QyxJQUFJLEVBQUU7d0JBRUosS0FBSyxFQUFFLG9CQUFvQjt3QkFFM0IsTUFBTSxFQUFFLGtDQUFrQzt3QkFDMUMsSUFBSSxFQUFFLEdBQUcsQ0FBQyxJQUFJO3FCQUNmO29CQUNELE1BQU0sRUFBRSxNQUFNO29CQUNkLE1BQU0sRUFBRSxFQUFFLGNBQWMsRUFBRSxrQkFBa0IsRUFBQztvQkFDN0MsT0FBTyxFQUFFLFVBQVMsU0FBYTt3QkFDM0IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQzt3QkFFckMsSUFBTSxJQUFJLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFBO3dCQUU1QyxJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUU7NEJBQ2YsRUFBRSxDQUFDLGNBQWMsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFBO3lCQUN6Qzs2QkFBTTs0QkFDTCxFQUFFLENBQUMsU0FBUyxDQUFDO2dDQUNYLEtBQUssRUFBRSxJQUFJLENBQUMsTUFBTTtnQ0FDbEIsSUFBSSxFQUFFLE1BQU07Z0NBQ1osUUFBUSxFQUFFLElBQUk7NkJBQ2YsQ0FBQyxDQUFBO3lCQUNIO29CQUVMLENBQUM7b0JBQ0QsSUFBSSxFQUFFLFVBQVMsS0FBSzt3QkFDaEIsT0FBTyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQzt3QkFDN0IsT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztvQkFDeEIsQ0FBQztpQkFDSCxDQUFDLENBQUE7WUFDSCxDQUFDO1NBQ0YsQ0FBQyxDQUFBO1FBRUUsRUFBRSxDQUFDLFVBQVUsQ0FBQztZQUNaLE9BQU8sRUFBRSxVQUFBLEdBQUc7Z0JBQ1YsSUFBSSxHQUFHLENBQUMsV0FBVyxDQUFDLGdCQUFnQixDQUFDLEVBQUU7b0JBRXJDLEVBQUUsQ0FBQyxXQUFXLENBQUM7d0JBQ2IsT0FBTyxFQUFFLFVBQUEsR0FBRzs0QkFFVixLQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsR0FBRyxHQUFHLENBQUMsUUFBUSxDQUFBOzRCQUl2QyxJQUFJLEtBQUksQ0FBQyxxQkFBcUIsRUFBRTtnQ0FDOUIsS0FBSSxDQUFDLHFCQUFxQixDQUFDLEdBQUcsQ0FBQyxDQUFBOzZCQUNoQzt3QkFDSCxDQUFDO3FCQUNGLENBQUMsQ0FBQTtpQkFDSDtZQUNILENBQUM7U0FDRixDQUFDLENBQUE7SUFDSixDQUFDO0NBQ0YsQ0FBQyxDQUFBIiwic291cmNlc0NvbnRlbnQiOlsiLy8gYXBwLnRzXG5BcHA8SUFwcE9wdGlvbj4oe1xuICBnbG9iYWxEYXRhOiB7fSxcbiAgb25MYXVuY2goKSB7XG4gICAgLy8g5bGV56S65pys5Zyw5a2Y5YKo6IO95YqbXG4gICAgLy8gY29uc3QgbG9ncyA9IHd4LmdldFN0b3JhZ2VTeW5jKCdsb2dzJykgfHwgW11cbiAgICAvLyBsb2dzLnVuc2hpZnQoRGF0ZS5ub3coKSlcbiAgICAvLyB3eC5zZXRTdG9yYWdlU3luYygnbG9ncycsIGxvZ3MpXG5cbiAgICAvLyDnmbvlvZVcbiAgICB3eC5sb2dpbih7XG4gICAgICBzdWNjZXNzOiByZXMgPT4ge1xuICAgIHd4LnJlcXVlc3Qoe1xuICAgICAgdXJsOiAnaHR0cDovLzQ3LjEwMi4xMTcuMjI0OjgwODAvZ2V0LW9wZW4taWQnLFxuICAgICAgZGF0YToge1xuICAgICAgICAgLy/loavkuIroh6rlt7HnmoTlsI/nqIvluo/llK/kuIDmoIfor4ZcbiAgICAgICAgYXBwaWQ6ICd3eDliNGY5MDIyMjg2NGQyNTcnLFxuICAgICAgICAgLy/loavkuIroh6rlt7HnmoTlsI/nqIvluo/nmoQgYXBwIHNlY3JldFxuICAgICAgICBzZWNyZXQ6ICc1YTg1MGNiYzYzNTc0MTgyOTlmOTAyYmU4NmVmZjQwOCcsXG4gICAgICAgIGNvZGU6IHJlcy5jb2RlXG4gICAgICB9LFxuICAgICAgbWV0aG9kOiAnUE9TVCcsXG4gICAgICBoZWFkZXI6IHsgJ2NvbnRlbnQtdHlwZSc6ICdhcHBsaWNhdGlvbi9qc29uJ30sXG4gICAgICBzdWNjZXNzOiBmdW5jdGlvbihvcGVuSWRSZXM6YW55KXtcbiAgICAgICAgICBjb25zb2xlLmxvZyhyZXMuY29kZSxvcGVuSWRSZXMuZGF0YSk7XG5cbiAgICAgICAgICBjb25zdCBkYXRhID0gSlNPTi5wYXJzZShvcGVuSWRSZXMuZGF0YS5kYXRhKVxuICAgICAgICAgIFxuICAgICAgICAgIGlmIChkYXRhLm9wZW5pZCkge1xuICAgICAgICAgICAgd3guc2V0U3RvcmFnZVN5bmMoJ29wZW5pZCcsIGRhdGEub3BlbmlkKVxuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICB3eC5zaG93VG9hc3Qoe1xuICAgICAgICAgICAgICB0aXRsZTogZGF0YS5lcnJtc2csXG4gICAgICAgICAgICAgIGljb246ICdub25lJyxcbiAgICAgICAgICAgICAgZHVyYXRpb246IDIwMDBcbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgfVxuICAgICAgICAgXG4gICAgICB9LFxuICAgICAgZmFpbDogZnVuY3Rpb24oZXJyb3IpIHtcbiAgICAgICAgICBjb25zb2xlLmluZm8oXCLojrflj5bnlKjmiLdvcGVuSWTlpLHotKVcIik7XG4gICAgICAgICAgY29uc29sZS5pbmZvKGVycm9yKTtcbiAgICAgIH1cbiAgIH0pXG4gIH0sXG59KVxuICAgIC8vIOiOt+WPlueUqOaIt+S/oeaBr1xuICAgIHd4LmdldFNldHRpbmcoe1xuICAgICAgc3VjY2VzczogcmVzID0+IHtcbiAgICAgICAgaWYgKHJlcy5hdXRoU2V0dGluZ1snc2NvcGUudXNlckluZm8nXSkge1xuICAgICAgICAgIC8vIOW3sue7j+aOiOadg++8jOWPr+S7peebtOaOpeiwg+eUqCBnZXRVc2VySW5mbyDojrflj5blpLTlg4/mmLXnp7DvvIzkuI3kvJrlvLnmoYZcbiAgICAgICAgICB3eC5nZXRVc2VySW5mbyh7XG4gICAgICAgICAgICBzdWNjZXNzOiByZXMgPT4ge1xuICAgICAgICAgICAgICAvLyDlj6/ku6XlsIYgcmVzIOWPkemAgee7meWQjuWPsOino+eggeWHuiB1bmlvbklkXG4gICAgICAgICAgICAgIHRoaXMuZ2xvYmFsRGF0YS51c2VySW5mbyA9IHJlcy51c2VySW5mb1xuXG4gICAgICAgICAgICAgIC8vIOeUseS6jiBnZXRVc2VySW5mbyDmmK/nvZHnu5zor7fmsYLvvIzlj6/og73kvJrlnKggUGFnZS5vbkxvYWQg5LmL5ZCO5omN6L+U5ZueXG4gICAgICAgICAgICAgIC8vIOaJgOS7peatpOWkhOWKoOWFpSBjYWxsYmFjayDku6XpmLLmraLov5nnp43mg4XlhrVcbiAgICAgICAgICAgICAgaWYgKHRoaXMudXNlckluZm9SZWFkeUNhbGxiYWNrKSB7XG4gICAgICAgICAgICAgICAgdGhpcy51c2VySW5mb1JlYWR5Q2FsbGJhY2socmVzKVxuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9LFxuICAgICAgICAgIH0pXG4gICAgICAgIH1cbiAgICAgIH0sXG4gICAgfSlcbiAgfSxcbn0pIl19